define <128 x i1> @add(<128 x i1> %v1, <128 x i1> %v2) {
entry:
  %sum = add <128 x i1> %v1, %v2
  ret <128 x i1> %sum
}

define i32 @main() {
entry:
  br label %loop

loop:
  %i = phi i64 [ 7812500, %entry ], [ %i1, %loop ]
  %v1 = phi <128 x i1> [ <i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0,i1 0>, %entry ], [ %sum, %loop ]
  %v2 = phi <128 x i1> [ <i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1>, %entry ], [ %v2ctr, %loop ]

  %sum = call <128 x i1> @add(<128 x i1> %v1, <128 x i1> %v2)
  %v2ctr = call <128 x i1> @add(<128 x i1> %v2, <128 x i1> <i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1,i1 1>)
  %i1 = sub i64 %i, 1
  %cmp = icmp eq i64 %i1, 0
  br i1 %cmp, label %fin, label %loop

fin:

  ret i32 0
}
