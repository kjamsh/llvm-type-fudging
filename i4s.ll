define <32 x i4> @add(<32 x i4> %v1, <32 x i4> %v2) {
entry:
  %sum = add <32 x i4> %v1, %v2
  ret <32 x i4> %sum
}



define i32 @main() {
entry:
  br label %loop

loop:
  %i = phi i64 [ 7812500, %entry ], [ %i1, %loop ]
  %v1 = phi <32 x i4> [ zeroinitializer, %entry ], [ %sum, %loop ]
  %v2 = phi <32 x i4> [ <i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1>, %entry ], [ %v2ctr, %loop ]

  %sum = call <32 x i4> @add(<32 x i4> %v1, <32 x i4> %v2)
  %v2ctr = call <32 x i4> @add(<32 x i4> %v2, <32 x i4> <i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1, i4 1>)
  %i1 = sub i64 %i, 1
  %cmp = icmp eq i64 %i1, 0
  br i1 %cmp, label %fin, label %loop

fin:

  ret i32 0
}
