#!/usr/bin/env sh

cd build

make

RESULT=$?

cd ..

if [[ RESULT -eq 0 ]]
then
  echo "=== Operating on in.ll ==="
  echo "Running pass"
  build/bin/swar in.ll -o out.ll 
  
  RESULT=$?
  if [[ RESULT -eq 0 ]]
  then 
    echo "Pass successful."
    echo "Compiling..."
    llc out.ll
    llc in.ll
    gcc out.s -o swar
    gcc in.s -o raw
    echo "Finished."
  fi

  echo "=== Operating on i1s.ll ==="
  echo "Running pass"
  build/bin/swar i1s.ll -o o1s.ll 
  
  RESULT=$?
  if [[ RESULT -eq 0 ]]
  then 
    echo "Pass successful."
    echo "Compiling..."
    llc o1s.ll
    llc i1s.ll
    gcc o1s.s -o swar1s
    gcc i1s.s -o raw1s
    echo "Finished."
  fi

  echo "=== Operating on i2s.ll ==="
  echo "Running pass"
  build/bin/swar i2s.ll -o o2s.ll 
  
  RESULT=$?
  if [[ RESULT -eq 0 ]]
  then 
    echo "Pass successful."
    echo "Compiling..."
    llc o2s.ll
    llc i2s.ll
    gcc o2s.s -o swar2s
    gcc i2s.s -o raw2s
    echo "Finished."
  fi

  echo "=== Operating on i4s.ll ==="
  echo "Running pass"
  build/bin/swar i4s.ll -o o4s.ll 
  
  RESULT=$?
  if [[ RESULT -eq 0 ]]
  then 
    echo "Pass successful."
    echo "Compiling..."
    llc o4s.ll
    llc i4s.ll
    gcc o4s.s -o swar4s
    gcc i4s.s -o raw4s
    echo "Finished."
  fi

  #rm out.ll in.s out.s o1s.ll i1s.s o1s.s o2s.ll i2s.s o2s.s o4s.ll i4s.s o4s.s

fi
