\documentclass[journal]{IEEEtran}
%
% If IEEEtran.cls has not been installed into the LaTeX system files,
% manually specify the path to it like:
% \documentclass[journal]{../sty/IEEEtran}

% *** CITATION PACKAGES ***
%
%\usepackage{cite}
% cite.sty was written by Donald Arseneau
% V1.6 and later of IEEEtran pre-defines the format of the cite.sty package
% \cite{} output to follow that of the IEEE. Loading the cite package will
% result in citation numbers being automatically sorted and properly
% "compressed/ranged". e.g., [1], [9], [2], [7], [5], [6] without using
% cite.sty will become [1], [2], [5]--[7], [9] using cite.sty. cite.sty's
% \cite will automatically add leading space, if needed. Use cite.sty's
% noadjust option (cite.sty V3.8 and later) if you want to turn this off
% such as if a citation ever needs to be enclosed in parenthesis.
% cite.sty is already installed on most LaTeX systems. Be sure and use
% version 5.0 (2009-03-20) and later if using hyperref.sty.
% The latest version can be obtained at:
% http://www.ctan.org/pkg/cite
% The documentation is contained in the cite.sty file itself.

\usepackage{url}

\usepackage{algorithmic}
\usepackage{algorithm}

\begin{document}

\title{CMPT 489 Course Project - SWARization}
\author{Kasra Jamshidi, James Hannah, Brendan Chan}

\maketitle

\section{Introduction and Motivation}

The Single Instruction Multiple Data paradigm is gaining popularity in modern processor development. These intructions consider large registers of 128-bits or more as vectors of smaller elements and operate on them in parallel. Using these instructions leads to a far greater degree of parallelism and can speed up CPU-bound tasks by orders of magnitude. An issue in exploiting these instructions is that they operate on fixed sizes of data elements, such as bytes in a 128-bit register. LLVM, however, allows the programmer to express any size field widths in its intermediate representation. This requires that LLVM translate from its incredibly expressive IR to the more restrictive instructions that the processor can actually perform. Currently, the strategy for legalizing unsupported SIMD types involves a great deal of casts and masks to ensure that the observed behaviour is what the programmer expects, at a large cost to efficiency.

This is an issue because some problems are more naturally expressed as using vectors with field widths of length 1, 2 or 4, which have no native processor support. Forcing the programmer to rephrase algorithms to match backend implementations does not mix with the expressive nature of the LLVM IR. In this paper we propose an LLVM pass that legalizes unsupported SIMD types with power-of-two field widths at zero cost and implement common arithmetic operations on these types using SIMD Within A Register (SWAR) techniques. Thus, the programmer benefits both from LLVM's expressive types and from the efficiency of processor-supported SIMD instructions.

\section{Design}

Our technique is based on three phases. First, we identify all instructions making use of illegal types and ensure we are capable of legalizing them. If an instruction is found at this stage that cannot be legalized, the pass terminates without modifying the source. Otherwise, we translate all operations on unsupported types into operations that can be performed on supported types. Finally, we transform all types into legal ones transparently. Our design rests on the insight that as expressive as LLVM types are, registers are untyped and can be treated as bit vectors.

\subsection{Operator Legalization}

The operator legalization pass works by iterating over all instructions in the LLVM module and replacing direct operations on types that aren’t representable in the instruction set with calls to functions specialized for performing the operation on that type.
This is made possible by dynamically constructing the function at runtime based on the type and the operation, using generic algorithms for field widths 2 and 4.
The \texttt{i1} case is special due to how SIMD arithmetic is specified. Only one bit is output, so arithmetic operations such as addition and multiplication can be directly replaced with bitwise logical operations.

These operations are more efficient than the generic, scalable functions constructed for types like \texttt{i2} and \texttt{i4}, and are guaranteed to be correct to the requirements of SIMD arithmetic.

\subsection{Type Legalization}

We leverage the fact that at runtime, LLVM types do not matter. We identify the minimum legal field width (typically \texttt{i8}) and transform all vectors with ``illegal'' field widths into this minimum. In essence, we erase the original types. The previous operator legalization phase takes into account the final type when generating SWAR code. We can do so safely because our initial program analysis showed that all instructions containing illegal types have a known legalization.

\section{Implementation Details}
The operation replacement pass replaces operations on vectors with function calls to methods that use swar techniques. The key concept to the techniques used involves separating the single binary vector operation into two vector operations with half the values each, but double the field width. The advantages to this approach include larger working space within the vector itself, and gives padding room between vector elements to prevent operations from "bleeding" into other elements.

\subsection{Replacement Algorithms}

Algorithms for vectors of \texttt{i1} values are incredibly simple in that they don't require masking values out and back in or replicating the register.
All arithmetic at this level can be replaced with a simple bit-wise operation.
This takes advantage of the fact that SIMD operations are expected to simply truncate to the length of the field, so there are no carries to think about.

On the other hand, algorithms for \texttt{i2} and \texttt{i4} require moving things around so as to ensure that arithmetic on one set of values does not affect arithmetic on a different set, even if they occupy the same larger field in the legalized register.
The generalized add function, for example, works by masking out alternating values from each input vector and performing two adds on each “half”, then recombining the resulting vectors using similar masks.

\subsubsection{Addition and Subtraction}
Addition is used as a core for other operations used here. For vectors of field width of 1, we can simply use an XOR operation on the whole vector.
Addition of two $<m x in>$ vectors goes as follows.
Separate each of the operand vectors into two vectors $a$ and $b$, of size $<m/2 x in*2>$, with a containing the odd indexed elements and b containing the even indexed elements. This can be done using masks and shifting, and can be done in constant time regardless of vector size.
Then, you can use addition operation on each a vector and each b vector to obtain the sum for the odd and even elements separately. It should be noted that it is possible for the output of addition for each element may have a size of $n+1$ bits, and therefore it is necessary to use a mask to reset all of the high bits of each element to 0.
From here you can shift and recombine the two sum vectors a and b to get the completed addition.
Subtraction can be done similarly by replacing the add with an subtract, and clearing underflow.

\subsubsection{Multiplication}
Multiplication of vectors $x$ and $y$ is tricky because you cannot directly use a multiply operation on the whole vector to produce an answer. Instead we simulate a multiply using many add instructions. First we separate each vector into the two vectors each containing the even and odd indexed elements. Then we create n vectors by left shifting over each element by 1 bit, effectively multiplying each element by two each time, such that vector \texttt{x[1]} is $x$ shifted by 1 bit, and \texttt{x[2]} is x shifted by 2 bits, and so on. Then we isolate the ith bit of each y vector, and copy that bit across the entire field width, such that \texttt{y[1]} is just has the first bit of each element, and \texttt{y[2]} has the second bit of each element, and so on. Now we can use each y vector as a mask for each x vector, and then simply add all the x vectors up to acquire our solution.

\subsubsection{Division and Remainder}
Division of vectors $x$ and $y$ can be implemented as an inverse multiply. Similarly to multiply, we separate the initial $x$ and $y$ into two subproblems consisting of the odd elements and even elements with doubled field width. Generate $n$ vectors of $y$ by left shifting over 1 bit each time. Then subtract each $y$ from $x$ if the operation does not result in underflow, that is, $x > y$. If \texttt{y[i]} can be subtracted from $x$, then we know the $i$th bit should be set in our answer, and we build the answer this way. Once all the $y$ vectors are compared with x, whatever remains of $x$ is the remainder.

\section{Implementation Evaluation}

Evaluation was performed on a 2.53GHz Intel Core i5 540-M running Arch Linux. We simulate processing a gigabyte of data by performing arithmetic operations on 128-bit vectors in a tight loop of 7812500 iterations.

\begin{algorithm}[h]
  \caption{Evaluation Script}\label{test}
  \begin{algorithmic}[1]
    \STATE{int i = 0}
    \STATE{vector v1 = $<$0, 0, \dots, 0$>$}
    \STATE{vector v2 = $<$1, 1, \dots, 1$>$}
    \REPEAT
      \STATE{v1 = v1 + v2}
      \STATE{v2 = v2 + $<$1, 1, \dots, 1$>$}
      \STATE{i = i + 1}
    \UNTIL{i = 7812500}
  \end{algorithmic}
\end{algorithm}

We evaluate the running time, compilation time, and post-compilation binary size of Algorithm~\ref{test} with and without our pass using $<$128 x \texttt{i1}$>$, $<$64 x \texttt{i2}$>$, and $<$32 x \texttt{i4}$>$ as the vector types.\footnote{The input files are in the appendix as \texttt{i1s.ll, i2s.ll}, and \texttt{i4s.ll}, respectively.}

As the results show, our technique vastly improves performance for all relevant metrics. Table \ref{runtime} shows the SWAR techniques provide a 125x running time speedup when manipulating bit vectors compared to LLVM's general approach. This speedup decreases linearly as the field width increases, but even with vectors of \texttt{i4}'s, our technique improves performance by a factor of 25. A key observation is that our technique does not suffer performance hits as field widths change and so scales linearly with vector width, whereas the LLVM technique scales quadratically (field width multiplied by vector width). The numbers in Table \ref{runtime} were obtained by running the popular benchmarking software \texttt{perf} against both the compiled binary of the initial input file and the compiled binary of the output of our pass.

\begin{table}[h]
  \caption{Mean Running Time (seconds): 5 trials}\label{runtime}
  \begin{center}
  \begin{tabular}{c | c c c}
    & \texttt{i1} & \texttt{i2} & \texttt{i4} \\
     \hline
     SWAR & $0.05 \pm 0.00$ & $0.07 \pm 0.00$ & $0.06 \pm 0.00$ \\
     Raw  & $6.26 \pm 0.01$ & $3.00 \pm 0.00$ & $1.50 \pm 0.00$ 
  \end{tabular}
  \end{center}
\end{table}

An important metric to consider is compilation time, as this can make or break the applicability of SIMD approaches in JIT compiled systems. Our technique also improves compilation time by orders of magnitude, as seen in Table \ref{compiletime}. We measure the time it takes to compile the raw input file versus the time it takes to apply our pass and then compile. Our all-or-nothing approach ensures that our pass is efficient. LLVM can take nearly two seconds to compile even a small module that makes use of \texttt{i1} vectors. Again, LLVM's performance decreases linearly as the field width does, whereas our technique's performance stays more stable.

\begin{table}[h]
  \caption{Mean Compilation Time (seconds): 5 trials}\label{compiletime}
  \begin{center}
  \begin{tabular}{c | c c c}
    & \texttt{i1} & \texttt{i2} & \texttt{i4} \\
     \hline
     SWAR & $0.05 \pm 0.00$ & $0.04 \pm 0.00$ & $0.03 \pm 0.00$ \\
     Raw  & $1.78 \pm 0.00$ & $0.88 \pm 0.00$ & $0.42 \pm 0.00$ 
  \end{tabular}
  \end{center}
\end{table}

Finally, we consider the output binary size, which affects memory usage of VMs and JIT compilers. Even our tiny module manages to produce up to 32 kilobytes of machine code when compiled under LLVM, but after applying our pass this number drops to $8.1$ kilobytes. Once more, LLVM scales with the field width while our technique produces consistent binary sizes.

\begin{table}[h]
  \caption{Output Binary Size (kilobytes)}\label{binarysize}
  \begin{center}
  \begin{tabular}{c | c c c}
    & \texttt{i1} & \texttt{i2} & \texttt{i4} \\
     \hline
     SWAR & $8.1$ & $8.1$ & $8.1$ \\
     Raw  & $32$ & $20$ & $12$ 
  \end{tabular}
  \end{center}
\end{table}

\section{Conclusion}

We conclude that our pass improves the running time, compilation time, and binary size of programs that make use of SIMD instructions on vectors with non-standard field widths. Indeed, there is no loss because even the compilation time (including our pass) is faster with our pass than without.

\section{Lessons Learned and Future Work}

\subsection{Lessons}

Since this project was focused around how to translate types properly, a significant number of lessons focus on types in LLVM.
Primarily, modifying types is difficult when you don't fully understand the system at play.
There were quite a few times when we were stumped as to how to transform the types being represented, and every time it was due to a misunderstanding or a lack of understanding of how the types are integrated with LLVM as a whole.
Types, especially static types, are useful, but at this level they can also get in the way of doing the things we knew we wanted to do.

\subsection{Future Work}

Future work for this project involves implementing the code to work with more operators, as well as vector types that aren't based on powers of two evenly dividing a 128-bit register.
As it stands, the project can only convert operations on vectors that fit into a 128-bit register, and only if that register is split into groups of \texttt{i1}, \texttt{i2} or \texttt{i4}.
This allowed for streamlining a great deal of cases, since we knew exactly our target type.
Some work should be put in to see if \texttt{<16 x i8>} is the most efficient target for this particular case.

In addition to this, working out how to extend our methods to work for 256-bit registers or even larger ones would be incredibly valuable.

Our methods also assume that the vector exists on its own, not inside another type.
The extension to allow for such an imbedded vector should be relatively simple, as this just involves a recursive check into other composite types.
However, it is even more true that we must be careful to make sure that our transformations keep everything working smoothly.
Having a part of a struct fail can be harder to catch than just a bare vector failing the same way.

\appendices
\section{Algorithms}
The original algorithms are written in C in the \url{c_proto} directory in the included zip file

\section{Test data}
The test programs used are \url{i1s.ll}, \url{i2s.ll}, and \url{i4s.ll} in the included zip file. Read the README for instructions on how to automatically generate raw and SWARized binaries using \url{test.sh}.

\end{document}
