SWAR
====

This uses `cmake` to build a makefile to build the project.

To build:

    # make a build directory
    $ mkdir $BUILD_DIR
    $ cd $BUILD_DIR
    # run cmake
    $ cmake -DLLVM_DIR=</path/to/LLVM/build>/lib/cmake/llvm $SOURCE_DIR
    # run make
    $ make

You can also build with `ninja` using the `-G Ninja` flag.

After building you can run `swar` manually:
    $ bin/swar in.ll -o out.ll

We also have a test script, `test.sh` that you can use to easily generate comparison binaries of our example inputs:
    $ cp i1s.ll in.ll
    $ ./test.sh
    $ ./raw  # compiled without our pass
    $ ./swar # compiled with our pass

Layout
------

The project layout is based on [LLVM class projects by Nick Sumner](https://github.com/nsumner/overflower-template).
It is laid out as follows:

- The `include` directory holds relevant header files.
- The `lib` directory contains the implementation of the pass and is the main driving code.
- The `tools` directory contains code for constructing a binary that will run our pass on given LLVM IR.
- The `c_proto` directory contains implementations of the algorithms used to mimic SIMD arithmetic written in C using an integer base type.
- `in.ll` is the input file used for testing the pass.
