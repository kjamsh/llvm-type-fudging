define <64 x i2> @add(<64 x i2> %v1, <64 x i2> %v2) {
entry:
  %sum = add <64 x i2> %v1, %v2
  ret <64 x i2> %sum
}

define i32 @main() {
entry:
  br label %loop

loop:
  %i = phi i64 [ 7812500, %entry ], [ %i1, %loop ]
  %v1 = phi <64 x i2> [ zeroinitializer, %entry ], [ %sum, %loop ]
  %v2 = phi <64 x i2> [ <i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1>, %entry ], [ %v2ctr, %loop ]

  %sum = call <64 x i2> @add(<64 x i2> %v1, <64 x i2> %v2)
  %v2ctr = call <64 x i2> @add(<64 x i2> %v2, <64 x i2> <i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1, i2 1>)
  %i1 = sub i64 %i, 1
  %cmp = icmp eq i64 %i1, 0
  br i1 %cmp, label %fin, label %loop

fin:

  ret i32 0
}

