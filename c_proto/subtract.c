#include <stdio.h>

#define VECTOR unsigned short

VECTOR sub (VECTOR x, VECTOR y); // swar add <4 x i4>

int main (void) {
	VECTOR a = 0xabcd;
	VECTOR b = 0x1234;
	VECTOR ans = add (a, b);

	printf("%.4x + %.4x =\n%.4x\n", a, b, ans);
}

VECTOR sub (VECTOR x, VECTOR y) {

	// split
	VECTOR y_odd  =  y & 0x0F0F;
	VECTOR y_even = (y & 0xF0F0) >> 4;

	VECTOR x_odd  = x & 0x0F0F;
	VECTOR x_even = (x & 0xF0F0) >> 4;

	// add
	VECTOR a = y_odd - x_odd;
	VECTOR b = y_even - x_even;

	// clean potential overflow
	a = a & 0x0F0F;
	b = b & 0x0F0F;

	return a | (b << 4);
}
