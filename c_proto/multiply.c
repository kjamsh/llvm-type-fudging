#include <stdio.h>

#define VECTOR unsigned short

int multiply (VECTOR x, VECTOR y, VECTOR *ans);
VECTOR multiply_low (VECTOR x, VECTOR y);
VECTOR multiply_high (VECTOR x, VECTOR y);
VECTOR gen_mask (VECTOR x, int y);

int main (void) {

	VECTOR a = 0xabcd;
	VECTOR b = 0x1234;
	VECTOR low_ans = multiply_low (a, b);
	VECTOR high_ans = multiply_high (a, b);
	printf("%.4x * %.4x =\nhigh\t%.4x\nlow\t%.4x\n", a, b, high_ans, low_ans);
}



int multiply (VECTOR x, VECTOR y, VECTOR *ans) {
	VECTOR a[4];
	VECTOR b[4];

	VECTOR y_odd  =  y & 0x0F0F;
	VECTOR y_even = (y & 0xF0F0) >> 4;

	VECTOR x_odd  = x & 0x0F0F;
	VECTOR x_even = (x & 0xF0F0) >> 4;

	VECTOR a_ans = 0;
	VECTOR b_ans = 0;

	int i = 4;
	while (i--) {
		a[i] = y_odd << i;
		b[i] = y_even << i;

		printf ("%d\n", i);
		printf ("num\t%.4x\t%.4x\n", x_odd, x_even);
		printf ("shift\t%.4x\t%.4x\n", a[i], b[i]);

		VECTOR mask_a = gen_mask(x_odd, i);
		a[i] = a[i] & mask_a;
		a_ans = a_ans + a[i];

		VECTOR mask_b = gen_mask(x_even, i);
		b[i] = b[i] & mask_b;
		b_ans = b_ans + b[i];
		printf ("mask\t%.4x\t%.4x\n", mask_a, mask_b);
		printf ("blended\t%.4x\t%.4x\n", a[i], b[i]);
		printf("ans\t%.4x\t%.4x\n", a_ans, b_ans);
	}

	ans[0] = a_ans;
	ans[1] = b_ans;
	return 0;
}

VECTOR multiply_high (VECTOR x, VECTOR y) {
	VECTOR ans[2];
	multiply (x, y, ans);
	ans[0] = (ans[0] & 0xF0F0) >> 4;
	ans[1] = (ans[1] & 0xF0F0);
	return ans[0] | ans[1];
}

VECTOR multiply_low (VECTOR x, VECTOR y) {
	VECTOR ans[2];
	multiply (x, y, ans);
	ans[0] = (ans[0] & 0x0F0F);
	ans[1] = (ans[1] & 0x0F0F) << 4;
	return ans[0] | ans[1];
}

VECTOR gen_mask(VECTOR x, int y) {
	printf ("gen\t%.4x\t%.4x\n", x, y);

	VECTOR a = (x & (0x0101 << y)) >> y;
	a = a | (a << 1) | (a << 2) | (a << 3) | (a << 4) | (a << 5) | (a << 6) | (a << 7);
}
