#include <stdio.h>

#define VECTOR unsigned short

VECTOR divide (VECTOR x, VECTOR y); //swar divide <4 x i4>
VECTOR gen_mask (VECTOR x, VECTOR y);    //swar test greater than

int main (void) {

	VECTOR a = 0xabcd;
	VECTOR b = 0x1234;
	VECTOR ans = divide (a, b);

	printf("%.4x / %.4x =\n%.4x\n", a, b, ans);
}

VECTOR divide (VECTOR x, VECTOR y) {
	VECTOR a[4];  // shifted values for odd elements of y
	VECTOR b[4];  // shifted values for even elements of y
	VECTOR ans = 0;

	VECTOR y_odd  =  y & 0x0F0F;
	VECTOR y_even = (y & 0xF0F0) >> 4;

	VECTOR x_odd  = x & 0x0F0F;
	VECTOR x_even = (x & 0xF0F0) >> 4;

	int i = 4; // because element length is 4
	while (i--) {
		a[i] = y_odd << i;
		b[i] = y_even << i;

		printf ("%d\n", i);
		printf ("num\t%.4x\t%.4x\n", x_odd, x_even);
		printf ("shift\t%.4x\t%.4x\n", a[i], b[i]);

		VECTOR mask_a = gen_mask(x_odd, a[i]);
		a[i] = a[i] & mask_a; // blend out elements we dont want
		x_odd = x_odd - a[i];
		ans = ans | ((0x0101 << i) & mask_a); // update answer

		VECTOR mask_b = gen_mask(x_even, b[i]);
		b[i] = b[i] & mask_b;
		x_even = x_even - b[i];
		ans = ans | ((0x1010 << i) & mask_b);

		printf ("mask\t%.4x\t%.4x\n", mask_a, mask_b);
		printf ("blended\t%.4x\t%.4x\n", a[i], b[i]);
		printf ("ans\t%.4x\n\n", ans);

	}


	return ans;

}

// will compare <2 x i7> and return 0xFF if x - y does not result in underflow
VECTOR gen_mask (VECTOR x, VECTOR y) {
	VECTOR a = x | 0x8080;
	a = (a - y) & 0x8080;
	a = a | (a >> 1) | (a >> 2) | (a >> 3) | (a >> 4) | (a >> 5) | (a >> 6) | (a >> 7);
	return a;
}
