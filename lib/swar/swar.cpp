#include "swar.h"

#include <string>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <string>
#include <memory>

#include "llvm/IR/IRBuilder.h"
#include "llvm/Transforms/Utils/ValueMapper.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/Type.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"

#include "llvm/Support/raw_ostream.h" // llvm::outs(), for debugging

#define PRINT(V)\
  do{(V)->print(llvm::outs()); llvm::outs() << '\n';}while(false)

static unsigned MIN_ELEMENT_SIZE = 8;

namespace swar {
char SwarizePass::ID = 0;
//char ReplacementPass::ID = 0;

bool isValidType(llvm::Type *t) {
  if (llvm::dyn_cast<llvm::VectorType>(t)) {
    unsigned scalarSz = t->getScalarSizeInBits();
    switch (scalarSz) {
      case 1:
      case 2:
      case 4:
        return false;
      default:
        return true;
    }
  }

  return true;
}

/**
 * General function to decide whether an instruction should be legalized.
 * Right now it just checks if we have a SWAR implementation for it but it could
 * be smarter, e.g. using the other SWAR group's cost model.
 */
bool shouldLegalizeInstruction(llvm::Instruction *inst) {
  unsigned opcode = inst->getOpcode();
  switch (opcode) {
    case llvm::Instruction::Add:
    case llvm::Instruction::Xor:
    case llvm::Instruction::And:
    case llvm::Instruction::Or:
    case llvm::Instruction::LShr:
    case llvm::Instruction::Shl:
    case llvm::Instruction::Sub:
    case llvm::Instruction::Mul:
    case llvm::Instruction::UDiv:
    case llvm::Instruction::Call:
    case llvm::Instruction::Ret:
    case llvm::Instruction::PHI:
      return true;
    default:
      return false;
  }
}

/**
 * Wrapper to communicate how to legalize a function
 */
struct IllegalFunction {
  IllegalFunction(llvm::Function *f, std::unordered_set<unsigned> argsToLegalize, bool shouldLegalizeReturnType)
    : argsToLegalize(argsToLegalize), shouldLegalizeReturnType(shouldLegalizeReturnType) {
      this->func = f;
  }

  void merge(std::shared_ptr<IllegalFunction> const &other) {
    shouldLegalizeReturnType = shouldLegalizeReturnType || other->shouldLegalizeReturnType;
    for (unsigned argNo : other->argsToLegalize) {
      argsToLegalize.insert(argNo);
    }
  }

  std::string toString() {
    std::string out(func->getName());
    out += " || Legalize Return: ";
    out += shouldLegalizeReturnType ? "yes" : "no";
    out += " || Legalize Args: ";
    for (unsigned argNo : argsToLegalize) {
      out += std::to_string(argNo);
    }

    return out;
  }

  llvm::Function *func;
  // argNo of the legalizeable arguments, may be empty
  std::unordered_set<unsigned> argsToLegalize;
  // whether the function return value is legalizeable
  bool shouldLegalizeReturnType;
};


std::shared_ptr<IllegalFunction> checkParentFunction(llvm::Function *func, llvm::Instruction *inst) {
  std::unordered_set<unsigned> argsToLegalize;

  // if the instruction is a return, change the return type
  if (llvm::dyn_cast<llvm::ReturnInst>(inst)) {
    return std::make_shared<IllegalFunction>(func, argsToLegalize, true);
  }

  // check if the function arguments appear in the instruction.
  for (auto *operand : inst->operand_values()) {
    if (auto *arg = llvm::dyn_cast<llvm::Argument>(operand)) {
      if (!isValidType(arg->getType())) {
        unsigned argNo = arg->getArgNo();
        argsToLegalize.insert(argNo);
      }
    }
  }

  if (!argsToLegalize.empty()) {
    return std::make_shared<IllegalFunction>(func, argsToLegalize, false);
  }

  return nullptr;
}

/**
 * Iterates over the basic blocks in func to find all return statements and
 * return their values.
 */
std::vector<llvm::Value *> getReturnValues(llvm::Function *func) {
  std::vector<llvm::Value *> result;
  for (auto &B : *func) {
    for (auto &I : B) {
      if (auto *ret = llvm::dyn_cast<llvm::ReturnInst>(&I)) {
        result.push_back(ret->getReturnValue());
      }
    }
  }

  return result;
}

/**
 * Creates a legal type from an illegal one, and returns it.
 */
llvm::VectorType *legalizeType(llvm::LLVMContext &ctx, llvm::Type *illegalType) {
  unsigned registerSize = illegalType->getPrimitiveSizeInBits();
  // XXX: should be the minimum size that supports every operation we implement.
  // I don't know if we can determine it dynamically, but that would be nice.
  unsigned desiredElementSize = MIN_ELEMENT_SIZE;
  unsigned vectorSize = registerSize / desiredElementSize;

  llvm::IntegerType *scalarType = llvm::IntegerType::get(ctx, desiredElementSize);
  llvm::VectorType *legalType = llvm::VectorType::get(scalarType, vectorSize);

  return legalType;
}

/**
 * Creates a legal function based on illegalFunc's values and replaces the
 * wrapped function in illegalFunc with it.
 */
void legalizeFunction(llvm::Module *mod, std::shared_ptr<IllegalFunction> const &illegalFunc) {
  llvm::Function *func = illegalFunc->func;
  //llvm::outs() << func->getName() << "\n";

  llvm::LLVMContext &ctx = func->getContext();

  bool shouldLegalizeReturnType = illegalFunc->shouldLegalizeReturnType;

  llvm::Type *newReturnType;
  llvm::Type *oldReturnType = func->getReturnType();
  if (shouldLegalizeReturnType) {
    newReturnType = legalizeType(ctx, oldReturnType);
  } else {
    newReturnType = oldReturnType;
  }

  // stores the indices in the function argument list of arguments that should
  // be legalized.
  std::unordered_set<unsigned> argsToLegalize = illegalFunc->argsToLegalize;

  llvm::FunctionType *oldType = func->getFunctionType();
  llvm::ArrayRef<llvm::Type *> oldParamTypesRef = oldType->params();

  // generate the backing array for the new params ArrayRef
  // XXX: memory leak
  llvm::Type **newParamTypes = new llvm::Type *[oldParamTypesRef.size()];
  for (unsigned i = 0; i < oldParamTypesRef.size(); i++) {
    newParamTypes[i] = argsToLegalize.count(i) == 1
      // only legalize if this argument's index is in argsToLegalize
      ? legalizeType(ctx, oldParamTypesRef[i])
      // otherwise stick with the original type
      : oldParamTypesRef[i];
  }

  // create the new (empty) function
  llvm::ArrayRef<llvm::Type *> newParamsRef(newParamTypes, oldType->params().size());
  llvm::FunctionType *newType = llvm::FunctionType::get(newReturnType, newParamsRef, false);
  llvm::Function *newFunc = llvm::Function::Create(newType, llvm::Function::ExternalLinkage, "", mod);

  // copy over the old arguments, legalizing the correct ones
  for (auto *newArg = newFunc->arg_begin(), *oldArg = func->arg_begin();
      oldArg != func->arg_end() && newArg != newFunc->arg_end();
      newArg++, oldArg++) {

    // make the new args take the old args' names.
    // not really necessary but it feels right
    newArg->takeName(oldArg);

    oldArg->replaceAllUsesWith(newArg);
  }

  //llvm::BasicBlock *newFuncEntry = llvm::BasicBlock::Create(newFunc->getContext(), "", newFunc);

  // move all basic blocks into new function
  std::vector<llvm::BasicBlock *> bbs;
  for (auto &bb: *func) { bbs.push_back(&bb); }
  for (auto *bb: bbs) {
    bb->removeFromParent();
    bb->insertInto(newFunc);
  }

  newFunc->takeName(func);
  func->replaceAllUsesWith(newFunc);

  func->eraseFromParent();
}

llvm::Constant *legalizeConstantVector(llvm::Constant *vector) {
  if (isValidType(vector->getType())) return vector;

  //auto desiredFieldSz = 8;
  auto desiredVectorSz = 16;
  auto desiredType = legalizeType(vector->getContext(), vector->getType());
  if (vector->isAllOnesValue()) {
    return llvm::Constant::getAllOnesValue(desiredType);
  } else if (vector->isNullValue()) {
    return llvm::Constant::getNullValue(desiredType);
  } else if (auto splat = vector->getSplatValue()) {
    unsigned typeSz = splat->getType()->getPrimitiveSizeInBits();
    unsigned sp = 0;
    switch (typeSz) {
      case 1:
        // groups of 8
        for (int i = 0; i < 8; i++) {
          unsigned c = *llvm::dyn_cast<llvm::ConstantInt>(vector->getAggregateElement(i))->getValue().getRawData();
          sp += c;
          sp = sp << 1;
        }
        break;
      case 2:
        // groups of 4
        for (int i = 0; i < 4; i++) {
          unsigned c = *llvm::dyn_cast<llvm::ConstantInt>(vector->getAggregateElement(i))->getValue().getRawData();
          sp += c;
          sp = sp << 2;
        }
        break;
      case 4:
        // groups of 2
        for (int i = 0; i < 2; i++) {
          unsigned c = *llvm::dyn_cast<llvm::ConstantInt>(vector->getAggregateElement(i))->getValue().getRawData();
          sp += c;
          sp = sp << 4;
        }
        break;
    }

    auto apsp = llvm::APInt(8, sp);
    auto outType = llvm::Type::getIntNTy(vector->getContext(), 8);
    return llvm::ConstantDataVector::getSplat(desiredVectorSz, llvm::Constant::getIntegerValue(outType, apsp));
  } else {
    auto bitSize = vector->getType()->getScalarSizeInBits();
    auto groupSize = 8 / bitSize;
    uint8_t values[16] = { 0 };
    for (int i = 0; i < 16; i++) {
      for (unsigned j = 0; j < groupSize; j++) {
        unsigned c = *llvm::dyn_cast<llvm::ConstantInt>(vector->getAggregateElement(i + j))->getValue().getRawData();
        values[i] = values[i] + c;
        values[i] = values[i] << bitSize;
      }
    }
    return llvm::ConstantDataVector::get(vector->getContext(), values);
  }
}


/**
 * TODO: all other instructions
 * is there some general way to replace instructions?
 *
 * It looks like these just copy the old instruction, but this updates their
 * types, which otherwise remain illegal.
 */
void legalizeInstruction(llvm::Instruction *inst) {
  if (auto *call = llvm::dyn_cast<llvm::CallInst>(inst)) {

    unsigned numArgs = call->getNumOperands();
    llvm::Function *func;
    // last operand is the function
    if (auto *f = llvm::dyn_cast<llvm::Function>(call->getOperand(numArgs - 1))) {
      func = f;
    }

    // create a new argument arrayref
    llvm::Value **newArgs = new llvm::Value *[numArgs];
    for (unsigned i = 0; i < numArgs - 1; i++) {
      llvm::Value *operand = call->getOperand(i);
      newArgs[i] = operand;
      if (auto *x = llvm::dyn_cast<llvm::Constant>(operand)) {
        if (x->getType()->isVectorTy()) {
          //llvm::outs() << "A constant: " << *x->getType() << "\n";
          auto y = legalizeConstantVector(x);
          //llvm::outs() << "A new constnat: " << *y->getType() << "\n";
          newArgs[i] = y;
        }
      }
    }

    llvm::ArrayRef<llvm::Value *> newArgsRef(newArgs, numArgs - 1);

    llvm::CallInst *newCall = llvm::CallInst::Create(func, newArgsRef);
    newCall->takeName(call);

    llvm::ReplaceInstWithInst(call, newCall);

  } else if (auto *ret = llvm::dyn_cast<llvm::ReturnInst>(inst)) {
    llvm::Value *retVal = ret->getReturnValue();
    //if (auto *x = llvm::dyn_cast<llvm::Constant>(retVal)) {
    //  //llvm::outs() << "A constant: " << *x->getType() << "\n";
    //}

    llvm::ReturnInst *newRet = llvm::ReturnInst::Create(ret->getParent()->getContext(), retVal);
    llvm::ReplaceInstWithInst(ret, newRet);

  } else if (inst->isBinaryOp()) {
    auto *lhs = inst->getOperand(0);
    auto *rhs = inst->getOperand(1);

    if (auto *l = llvm::dyn_cast<llvm::Constant>(lhs)) {
      //llvm::outs() << "lhs: " << *l->getType() << "\n";
      lhs = legalizeConstantVector(l);
    }

    if (auto *r = llvm::dyn_cast<llvm::Constant>(rhs)) {
      //llvm::outs() << "rhs: " << *r->getType() << "\n";
      rhs = legalizeConstantVector(r);
    }

    llvm::BinaryOperator *newBinOp = llvm::BinaryOperator::Create(
        (llvm::Instruction::BinaryOps) inst->getOpcode(),
        lhs,
        rhs);

    llvm::ReplaceInstWithInst(inst, newBinOp);
  } else if (auto *phi = llvm::dyn_cast<llvm::PHINode>(inst)) {
    // XXX: icky but it works
    phi->mutateType(legalizeType(phi->getContext(), phi->getType()));
    for (unsigned i = 0; i < phi->getNumIncomingValues(); i++) {
      auto *val = phi->getIncomingValue(i);
      if (auto *constant = llvm::dyn_cast<llvm::Constant>(val)) {
        val = legalizeConstantVector(constant);
        phi->setIncomingValue(i, val);
      }
    }

  } else {
    //llvm::outs() << "FOUND AN UNKNOWN INSTRUCTION\n";
    //PRINT(inst);
  }
}

// turn an integer mask into a bit vector mask
std::tuple<llvm::Constant *, llvm::Constant *, llvm::Constant *> getVectorMask(llvm::LLVMContext &ctx, int elementSz, int vectorSz) {
  //int desiredEltSz = 8;
  std::vector<uint8_t> maskbits1;
  std::vector<uint8_t> maskbits2;
  uint8_t firstNum;
  uint8_t secondNum;
  llvm::Constant *vectorNum;
  switch (elementSz) {
    case 2:
      firstNum = 0xCC;
      secondNum = 0x33;
      vectorNum = llvm::Constant::getIntegerValue(llvm::Type::getInt8Ty(ctx), llvm::APInt(8, 2));
      break;
    case 4:
      firstNum = 0xF0;
      secondNum = 0x0F;
      vectorNum = llvm::Constant::getIntegerValue(llvm::Type::getInt8Ty(ctx), llvm::APInt(8, 4));
      break;
    default:
      throw "Should never be the case";
  }

  // XXX: hardcoded
  int desiredVectorSz = 16;

  for (int j = 0; j < desiredVectorSz; j++) {
    maskbits1.push_back(firstNum);
    maskbits2.push_back(secondNum);
  }

  auto vectorMask = llvm::ConstantDataVector::getSplat(desiredVectorSz, vectorNum);

  return std::make_tuple<>(llvm::ConstantDataVector::get(ctx, maskbits1), llvm::ConstantDataVector::get(ctx, maskbits2), vectorMask);
}

llvm::Function* getFunc(llvm::Module& m, llvm::Instruction::BinaryOps type, llvm::VectorType* vecType) {
  std::string name {"SwArIzE"}; // make name collisions unlikely
  auto vectorSz = vecType->getNumElements();
  auto fieldWidth = vecType->getScalarSizeInBits();
  switch (type) {
    case llvm::Instruction::Add:
      name += "add"; break;
    default:
      return nullptr;
  }
  name += std::to_string(vectorSz);
  name += "xi";
  name += std::to_string(fieldWidth);

  auto funcType = llvm::FunctionType::get(vecType, {vecType, vecType}, false);
  // later, check for body and insert appropriate one if needed
  auto fun = llvm::dyn_cast<llvm::Function>(m.getOrInsertFunction(name, funcType));
  assert(fun && "could not getOrInsertFunction");
  auto args = fun->arg_begin();
  auto x = args++;
  auto y = args++;
  assert(x && y && "arg pointer is null");
  x->setName("x");
  y->setName("y");

  if (fun->empty()) {
    // add body through dispatcher function
    llvm::BasicBlock* block = llvm::BasicBlock::Create(m.getContext(), "swar", fun);
    llvm::IRBuilder<> builder(block);
    switch (type) {
      case llvm::Instruction::Add:
      if (fieldWidth == 1){
        auto tmp = builder.CreateBinOp(llvm::Instruction::Xor, x, y, "tmp");
        builder.CreateRet(tmp);
      } else {
        /* don't ask */

        auto &ctx = m.getContext();
        auto maskTuple = getVectorMask(ctx, fieldWidth, vectorSz);
        auto m1 = std::get<0>(maskTuple);
        auto m2 = std::get<1>(maskTuple);
        auto vSz = std::get<2>(maskTuple);

        auto y_odd = builder.CreateBinOp(llvm::Instruction::And, y, m2);
        auto y_even1 = builder.CreateBinOp(llvm::Instruction::And, y, m1);
        auto y_even2 = builder.CreateBinOp(llvm::Instruction::LShr, y_even1, vSz);
        auto x_odd = builder.CreateBinOp(llvm::Instruction::And, x, m2);
        auto x_even1 = builder.CreateBinOp(llvm::Instruction::And, x, m1);
        auto x_even2 = builder.CreateBinOp(llvm::Instruction::LShr, x_even1, vSz);
        auto a = builder.CreateBinOp(llvm::Instruction::Add, x_odd, y_odd);
        auto b = builder.CreateBinOp(llvm::Instruction::Add, x_even2, y_even2);
        auto a_clean = builder.CreateBinOp(llvm::Instruction::And, a, m2);
        auto b_clean = builder.CreateBinOp(llvm::Instruction::And, b, m2);
        auto b_shift = builder.CreateBinOp(llvm::Instruction::Shl, b_clean, vSz);
        auto ret = builder.CreateBinOp(llvm::Instruction::Or, a_clean, b_shift);
        builder.CreateRet(ret);
      }
      break;
      case llvm::Instruction::Sub: {

        auto &ctx = m.getContext();
        auto maskTuple = getVectorMask(ctx, fieldWidth, vectorSz);
        auto m1 = std::get<0>(maskTuple);
        auto m2 = std::get<1>(maskTuple);
        auto vSz = std::get<2>(maskTuple);

        auto y_odd = builder.CreateBinOp(llvm::Instruction::And, y, m2);
        auto y_even1 = builder.CreateBinOp(llvm::Instruction::And, y, m1);
        auto y_even2 = builder.CreateBinOp(llvm::Instruction::LShr, y_even1, vSz);
        auto x_odd = builder.CreateBinOp(llvm::Instruction::And, x, m2);
        auto x_even1 = builder.CreateBinOp(llvm::Instruction::And, x, m1);
        auto x_even2 = builder.CreateBinOp(llvm::Instruction::LShr, x_even1, vSz);
        auto a = builder.CreateBinOp(llvm::Instruction::Sub, x_odd, y_odd);
        auto b = builder.CreateBinOp(llvm::Instruction::Sub, x_even2, y_even2);
        auto a_clean = builder.CreateBinOp(llvm::Instruction::And, a, m2);
        auto b_clean = builder.CreateBinOp(llvm::Instruction::And, b, m2);
        auto b_shift = builder.CreateBinOp(llvm::Instruction::Shl, b_clean, vSz);
        auto ret = builder.CreateBinOp(llvm::Instruction::Or, a_clean, b_shift);
        builder.CreateRet(ret);
      }
      break;
 
      case llvm::Instruction::Mul:
      if (fieldWidth == 1){
        auto tmp = builder.CreateBinOp(llvm::Instruction::And, x, y, "tmp");
        builder.CreateRet(tmp);
      }

      default:
      builder.CreateRetVoid();
    }
  }
  return fun;
}

bool legalizeOperator(llvm::BasicBlock &BB) {
  for (auto& I : BB) {
    auto *i = &I;
    if (i == nullptr) return false;
    if (auto BinOp = llvm::dyn_cast<llvm::BinaryOperator>(&I)) {
      auto lhs = BinOp->getOperand(0);
      auto rhs = BinOp->getOperand(1);
      auto lType = lhs->getType();
      auto rType = rhs->getType();
      if (lType->getTypeID() == rType->getTypeID() && lType->isSized()) {
        unsigned scalarSz = lType->getScalarSizeInBits();
        unsigned primitiveSz = lType->getPrimitiveSizeInBits();
        unsigned vectorSz = primitiveSz / scalarSz; // type is <vectorSz x iscalarSz>
        bool isVector = vectorSz != 1;

        if (/*vector size is one we cannot operate on or*/!isVector) { continue; }

        llvm::Instruction::BinaryOps op = BinOp->getOpcode();
        switch (op) {
          // insert opcode names here (need to figure out what they are)
          // supported opcodes; do not skip
          case llvm::Instruction::Add:
          // XXX: haven't actually implemented these yet
          //case llvm::Instruction::Sub:
          //case llvm::Instruction::Mul:
          //case llvm::Instruction::SDiv:
          //case llvm::Instruction::And:
          //case llvm::Instruction::Or:
          //case llvm::Instruction::Xor:
            break;
          default: op = (llvm::Instruction::BinaryOps) 0; break;
        }
        if (!op) { continue; }
        // callOurLibFunc should maintain a database of functions created so far, and create one if necessary
        // It should then make a call instruction to that function using lhs and rhs
        // Then we call llvm::ReplaceInstWithInst(&I, &call); to replace all references to the old value with the one this call makes
        // All types will be replaced in the future call to SwarizePass, so don't worry about that here
        // llvm::CallInst call = callOurLibFunc(op, lhs, rhs);
        llvm::VectorType *desired = llvm::dyn_cast<llvm::VectorType>(lType);
        assert(desired && "could not create vector type in given context");
        auto M = BB.getModule();
        assert(M && "could not access module");
        llvm::Function* fn = getFunc(*M, op, desired);
        assert(fn && "could not get function");
        auto call = llvm::CallInst::Create(fn, {lhs, rhs});
        llvm::ReplaceInstWithInst(&I, call);
      }
    }
  }
  return true;
}

bool SwarizePass::runOnModule(llvm::Module& M) {
  std::unordered_map<llvm::Function *, std::shared_ptr<IllegalFunction>> illegalFunctions;
  // order matters!!
  std::vector< llvm::Instruction *> illegalInstructions;
  std::vector< llvm::Instruction *> illegalReturnInstructions;

  // find all legalizeable nodes and store them
  //llvm::outs() << "Legalizing operators\n";
  for ( auto &F : M) {
    // don't bother changing anything if can't change everything.
    // so just find all illegal instructions and if you don't know how to handle
    // them just bail out.

    if (F.getName().str().substr(0, 7) == std::string("SwArIzE")) { continue; }
    //llvm::outs() << F.getName() << "\n";
    for (auto &B : F)  {
      legalizeOperator(B);
    }
  }

  //llvm::outs() << "Legalizing types\n";
  for (auto &F : M) {
    for (auto &B : F)  {
      for (auto &I : B) {
        // XXX: maybe some instructions whose types are valid have operands with
        // invalid types? In which case shouldn't just check I.getType() but all
        // operands' types too.
        if (!isValidType(I.getType())) {
          if (shouldLegalizeInstruction(&I)) {
            illegalInstructions.push_back(&I);
            // check if instruction requires its function to be modified
            if (std::shared_ptr<IllegalFunction> illegalFunction = checkParentFunction(&F, &I)) {
              try {
                std::shared_ptr<IllegalFunction> existingFunction = std::move(illegalFunctions.at(&F));
                illegalFunctions.erase(&F);
                illegalFunction->merge(existingFunction);
              } catch (std::exception e) {}
              illegalFunctions.insert({&F, std::move(illegalFunction)});
            }
          } else {
            //llvm::outs() << "can't legalize! " << I << "\n";
            return false;
          }
        } else if (I.getType()->isVoidTy()) {
          if (auto *ret = llvm::dyn_cast<llvm::ReturnInst>(&I)) {
            if (!ret->getReturnValue()->getType()->isVectorTy()) continue;
          }
          illegalReturnInstructions.push_back(&I);
          if (std::shared_ptr<IllegalFunction> illegalFunction = checkParentFunction(&F, &I)) {
            try {
              std::shared_ptr<IllegalFunction> existingFunction = std::move(illegalFunctions.at(&F));
              illegalFunctions.erase(&F);
              illegalFunction->merge(existingFunction);
            } catch (std::exception e) {}
            illegalFunctions.insert({&F, std::move(illegalFunction)});
          }
        }
      }
    }
  }

  // start legalizing!
  for (auto &&pair : illegalFunctions) {
    legalizeFunction(&M, pair.second);
  }

  // do all non-return instructions
  for (auto *inst : illegalInstructions) {
    legalizeInstruction(inst);
  }

  // repair returns
  for (auto *inst : illegalReturnInstructions) {
    legalizeInstruction(inst);
  }

  return true;
}

} /* end namespace */


static llvm::RegisterPass<swar::SwarizePass> X("swar", "Swarize Pass",
                             false /* Only looks at CFG */,
                             false /* Analysis Pass */);
