#pragma once

#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"

namespace swar {

struct SwarizePass : public llvm::ModulePass {
    static char ID;

    SwarizePass() : llvm::ModulePass(ID) {}

    bool runOnModule(llvm::Module&) override;

    //void getAnalysisUsage(llvm::AnalysisUsage &AU) const override;
    // anything else that should be in here
};

//struct ReplacementPass : public llvm::BasicBlockPass {
//    static char ID;
//
//    ReplacementPass() : llvm::BasicBlockPass(ID) {}
//
//    bool runOnBasicBlock(llvm::BasicBlock&) override;
//
//    llvm::Function* getFunc(llvm::Module&, llvm::Instruction::BinaryOps, llvm::VectorType&);
//
//};

}
